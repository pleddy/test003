m_md5 = require('md5')
Sentence = require('../models/sentence')
googleTranslate = require('google-translate')('AIzaSyBd_lpxM52rHwe613hiQ_Ig-_Jb66e2t8I');

exports.findSentenceTranslation = function(id, next) {
  Sentence.findOne({'md5': id}, function (err, m_sentence) {
    if (err) {
      console.log('Error:findSentenceTranslation:findOne')
      next(err, null)
    }
    if (!m_sentence) {
      console.log('Oh shit!')
    }
    next(null, m_sentence)
  })
}

exports.addSentence = function(m_string, m_string_md5, m_lang) {
  Sentence.findOne({'md5': m_string_md5}, function(err, m_sentence) {
      if (err) {
        console.log('Error:addSentence:findOne')
        return
      }
      if (!m_sentence) {
        googleTranslate.translate(
          m_string, 
          m_lang,
          function(err, m_translation) { // callback 001
            console.log(m_translation)
            console.log('gtrans: ' + m_translation.translatedText)
            if (!m_translation) {
              console.log('Trans failed to give translation')
              return
            }
            newSentence = new Sentence({
              content: m_string,
              md5: m_string_md5,
              translation: m_translation.translatedText
            })
            newSentence.save(function(err, m_sentence) {
              if (err) {
                return next(err)
              }
            })
          }
        )
      }
  }) 
}
