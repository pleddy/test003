Story = require('../models/story')
googleTranslate = require('google-translate')('AIzaSyBd_lpxM52rHwe613hiQ_Ig-_Jb66e2t8I');

exports.addStory = function(m_story, next) {
  console.log('My story: ', m_story)
  googleTranslate.detectLanguage(m_story.content, function(err, m_detection) {
    console.log('My detection: ', m_detection)
    if (err) {
      console.log(err)
      console.log('Error:googleTranslate:detectLanguage')
      next(err)
    }
    var newStory = new Story({
      title: m_story.title,
      content: m_story.content,
      _creator: m_story.creator_id,
      lang: m_detection.language
    })
    newStory.save(function(err) {
      if (err) return next(err)
      next(null)
    })
  })
}

exports.removeStory = function(id, next) {
  Story.remove( {_id: id}, function(err) {
    if (err) return next(err)
    next(null)
  })
}

