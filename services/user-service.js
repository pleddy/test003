User = require('../models/user')

exports.addUser = function(user, next) {
  var newUser = new User({
    email: user.email.trim()
  })

  newUser.save(function(err, m_user) {
    if (err) return next(err, null)
    next(null, m_user)
  })
}

exports.removeUser = function(id, next) {
  User.remove( {_id: id}, function(err) {
    if (err) return next(err)
    next(null)
  })
}

exports.findUser = function(id, next) {
  User.findOne( {_id: id}, function(err, m_user) {
    if (err) return next(err, null)
    next(null, m_user)
  })
}

