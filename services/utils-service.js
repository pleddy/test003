_ = require("underscore")

exports.parse_words = function(m_string) {
  m_string = m_string.replace(/[\.,-\/#!$%\^&\*;:{}=\-_`~()\'\"?¿¡—]/g,'')
  m_string = m_string.toLowerCase()
  m_words = m_string.split(/[\s,]+/)
  m_words = m_words.sort()
  m_words = _.uniq(m_words)
  m_words = m_words.filter(function(str) { return str.length >= 4; });
  //console.log('Parsed words: ' + m_words)
  return m_words
}

exports.parse_sentences = function(m_string, m_lang) {
  console.log('START')
//  console.log(m_string)
  //m_string = m_string + ' '
  //m_string = m_string.replace(/[‘]/g,'')
  //m_string = m_string.replace(/[-\/#$%\^&\*{}=_`~]/g,'')
  m_string = m_string.replace(/[“”]/g,'"')
  //m_string = m_string.replace(/\s+"\s+/g,'" ')
  //m_string = m_string.replace(/\s+"\s*\r\n/g,'"\r\n')

  // if no double returns, use single
  m_match = m_string.match(/\r\n\r\n/)
  if ( ! m_match ) {
    m_string = m_string.replace(/\r\n/g, '<p>')
  }

  m_string = m_string.replace(/\r\n\s*\r\n/g, '<p>')
  m_string = m_string.replace(/\r\n/g, ' ')
  m_string = m_string.replace(/\.{3}/g, ', ')
  //m_string = m_string.replace(/\s+/g, ' ')

  m_paragraphs = m_string.split(/<p>/g)
  //console.log(m_paragraphs)
  m_sentences_all = []
  for (var i=0, ilen=m_paragraphs.length; i < ilen; i++) {

    m_paragraph = m_paragraphs[i]

    if ( m_paragraph.match(/[—\-]/) && m_lang == 'es' ) {
      m_sentences_all.push(m_paragraph.trim())
      continue
    }
    
    // spanish exceptions
    //m_paragraph = m_paragraph.replace(/^—{1,2}([^—]+[\.?!"])$/, '"$1"')
    
    //console.log(m_paragraph)
    //m_sentences = m_paragraph.match(/.+?([\.?!])(?=\s)/g)
    m_sentences = m_paragraph.match(/.+?([\.?!])\s*/g)
    if ( m_sentences === null ) {
      //m_sentences_all.push(m_paragraph)
      console.log('WARNING: strange sentence ignored')
      continue
    }
    //console.log(m_sentences)
    for (var j=0, jlen=m_sentences.length; j < jlen; j++) {
      m_sentence = m_sentences[j].trim()
      m_sentences_all.push(m_sentence)
    }
  }
//  return m_paragraphs
  console.log(m_sentences_all)
  return m_sentences_all
}

exports.compare_percent = function (a,b) {
  if (a.m_percent < b.m_percent)
    return -1;
  if (a.m_percent > b.m_percent)
    return 1;
  return 0;
}