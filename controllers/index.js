User = require('../models/user')

module.exports = {

  index: function(req, res) {
    console.log(req.session)
    res.render('index', { user: req.user });
  },

  login: function(req, res) {
    User.findOne({ email: req.body.email }, function(err, user) {
        console.log(err)
        console.log(user)
        if (err) {
          console.log('error while finding user by email')
          return res.render('deadend')
        }
        if ( !user ) {
          console.log('Email: ', req.body.email)
          console.log('Pass: ', req.body.password)
          User.register(new User({ email: req.body.email }), req.body.password, function(err, user) {
              if (err) {
                console.log('myerror while user register!', err);
                return res.render('index', { alert: 'error while registering' });
              }
              if (!user) {
                console.log('no user created');
                return res.render('index', { alert: 'user was not created' });
              }
              User.findOne({ email: req.body.email }, function(err, user) {
                  req.login(user, function(err) {
                    console.log('Logging in... ')
                    if (err) {
                      console.log('Error: ', err)
                      return res.redirect('/');
                    }
                    console.log(req.session)
                    return res.redirect('/');
                  });
              })
          });
        } else {
          console.log('User already exists')
          user.authenticate(req.body.password, function(err, user, reason) {
            if (err) {
              console.log(err)
              return res.render('index', { alert: 'Error while authenticating' });
            }
            if (!user) {
              console.log(reason.message)
              res.render('index', { alert: reason.message });
            } else {
              console.log('Success: ' + user)
              req.login(user, function(err) {
                if (err) {
                  console.log(err)
                  return res.redirect('/');
                }
                res.redirect('/');
              });
            }
          })
        }
      }
    )
  },

  logout: function(req, res) {
    req.logout();
    res.redirect('/')
  }

}





