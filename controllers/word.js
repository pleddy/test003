//User = require('../models/user').User
userService = require('../services/user-service')

module.exports = {

  add: function(req, res) {
    m_sess = req.session;
    console.log("Sess story_id: " + m_sess.story_id)
    console.log("Word: " + req.params.id)
    console.log("Sess user_id: " + req.user.id)

    userService.findUser(req.user.id, function(err, m_user) {
      if (err) {
        console.log('Error:word:remove:findUser')
        return res.redirect('/stories/list')
      }
      console.log(m_user)
      m_user.words.addToSet(req.params.id)
      m_user.words.sort()
      m_user.save(function (err) {
        if (err) {
          return handleError(err) 
        }
        console.log('Success add!'); 
        res.redirect('/stories/view/' + m_sess.story_id + '/sentence/' + req.params.sentence_id) 
      })
    })
  },

  remove: function(req, res) {
    m_sess = req.session;
    userService.findUser(req.user.id, function(err, m_user) {
      if (err) {
        console.log('Error:word:remove:findUser')
        return res.redirect('/stories/list')
      }
      m_user.words.remove(req.params.id)
      m_user.save(function (err) {
        if (err) {
          console.log('Error:word:remove:findOne')
          return handleError(err) 
        }
        console.log('Success remove!'); 
        res.redirect('/stories/view/' + m_sess.story_id + '/sentence/' + req.params.sentence_id)
      })
    })

  }

}

