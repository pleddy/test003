Story = require('../models/story')
User = require('../models/user')
storyService = require('../services/story-service')
sentenceService = require('../services/sentence-service')
m_userservice = require('../services/user-service')
_ = require("underscore")
m_md5 = require('md5')
m_qs = require("querystring")
m_utils = require('../services/utils-service')

module.exports = {

  list: function(req, res) {
    m_sess = req.session;
    //console.log(req.user)
    Story.find(
      //{ _creator: sess.user_id },
      { _creator: req.user.id },
      function(err, stories) {
        if (err) {return res.render('stories/list') }
        m_stats = []
        _.each(stories, function(m_story) {
          m_words = m_utils.parse_words(m_story.content)
          m_stats.push({
              "m_story": m_story,
              "m_words": m_words,
              "m_percent": Math.round((_.intersection(m_words, req.user.words).length/m_words.length)*100),
          })
        })
        //console.log(m_stats)
        m_stats.sort(m_utils.compare_percent)
        m_stats.reverse()
        vm = {
          stories: stories,
          m_stats: m_stats
        }
        res.render('stories/list', { vm: vm, user: req.user });
      }
    )
  },

  new: function(req, res) {
    res.render('stories/new', { user: req.user });
  },

  view: function(req, res) {
    m_sess = req.session;
    //console.log("Story view sess: " + m_sess.words)

    console.log('Sentence id: ' + req.params.sentence_id)

    Story.findOne({ _id: req.params.id }, function(err, m_story) {
      if (err) {
        console.log('ERROR:story:view:findOne')
        return res.redirect('/stories/list', { user: req.user });
      }
      m_words = m_utils.parse_words(m_story.content)
      m_story.lang = ( m_story.lang ) ? m_story.lang : 'es'
      m_sentences = m_utils.parse_sentences(m_story.content, m_story.lang)
      _.each(m_sentences, function(m_sentence) {
        m_md5_string = m_md5(m_sentence)
        sentenceService.addSentence(m_sentence, m_md5_string, 'en')
      })

      // TO DO: detect lang if missing

      m_vm = {
        m_sentences: m_sentences,
        story: m_story,
        lang: 'es'
      }
      m_sess.story_id = m_story.id

      if (req.params.sentence_id) {
         sentenceService.findSentenceTranslation(req.params.sentence_id, function(err, m_sentence) {
          console.log('sentence found: ' + m_sentence)
          if (err) {
            console.log('ERROR:view:findSentenceTranslation')
          }
          if (!m_sentence) {
            m_vm.sentence = { content: '', translate: 'Translation FAILED'}
            res.render('stories/view', { vm: m_vm, user: req.user })
          }
          m_vm.sentence = m_sentence
          m_sentencewords = m_utils.parse_words(m_sentence.content)
          //console.log('Sentence words: ' + m_sentencewords)
          //console.log('User words: ' + req.user.words)

          m_vm.sentence_unknown_words = _.difference(m_sentencewords, req.user.words)
          m_vm.sentence_known_words = _.intersection(m_sentencewords, req.user.words)
          //console.log(m_vm.sentence_unknown_words)
          //console.log(m_vm.sentence_known_words)
          res.render('stories/view', { 
            vm: m_vm, 
            user: req.user 
          })
        })
      } else {
        res.render('stories/view', { vm: m_vm, user: req.user })
      }

    })
  },
  view_sentences: function(req, res) {
    m_sess = req.session;
    Story.findOne({ _id: req.params.id }, function(err, m_story) {
      if (err) {return res.redirect('/stories/list', { user: req.user }); }
      m_story.lang = ( m_story.lang ) ? m_story.lang : 'es'
      m_sentences = m_utils.parse_sentences(m_story.content, m_story.lang)
      m_vm = {
        m_sentences: m_sentences,
        m_story: m_story,
        lang: 'es'
      }
      res.render('stories/view_sentences', { vm: m_vm, user: req.user })
    })
  },

  remove: function(req, res) {
    storyService.removeStory(req.params.id, function(err) {
      if (err) {
        return res.redirect('/stories/list');
      }
      res.redirect('/stories/list');
    })
  },

  add: function(req, res) {
    story = {}
    story.title = req.body.title.trim()
    story.content = req.body.content.trim()
    story.lang = req.body.lang
    story.creator_id = req.body.user_id
    storyService.addStory(story, function(err) {
      if (err) {
        console.log('ERROR:add:storyService:addStory')
        return res.redirect('/stories/list')
      }
      res.redirect('/stories/list')
    })
  }
}

