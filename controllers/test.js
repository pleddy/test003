m_utils = require('../services/utils-service')

module.exports = {

  run: function(req, res) {
    m_sentences = m_utils.parse_sentences(  "Trino tenía que correr o morir.\r\n\r\n")
    vm = {
      m_sentences: m_sentences
    }
    res.render('tests/run', { vm: vm, user: req.user });
  },

}

