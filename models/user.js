var validate = require('mongoose-validate');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

var User = new Schema ({
    email: { type: String, required: true, validate: [validate.email, 'invalid email address'] },
    words: { type: [String], index: true }
});

User.plugin(passportLocalMongoose, {usernameField: 'email'});

module.exports = mongoose.model('User', User);
