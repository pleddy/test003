mongoose = require('mongoose')
Schema = mongoose.Schema

Sentence = new Schema ({
  content : { type: String, required: true },
  md5 : { type: String, index: true, required: true },
  translation : String,
})

module.exports = mongoose.model('Sentence', Sentence)

