mongoose = require('mongoose')
Schema = mongoose.Schema

Story = new Schema ({
  _creator : { type: Schema.Types.ObjectId },
  title : { type: String, required: true },
  content : { type: String, required: true },
  lang : { type: String }
})

module.exports = mongoose.model('Story', Story)
