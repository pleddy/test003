var express = require('express');
var router = express.Router();
var controller_word = require('../controllers/word')

router.get('/add/:id', controller_word.add );
router.get('/add/:id/sentence/:sentence_id', controller_word.add );
router.get('/remove/:id', controller_word.remove );
router.get('/remove/:id/sentence/:sentence_id', controller_word.remove );

module.exports = router;
