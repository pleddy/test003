var express = require('express');
var router = express.Router();
var controller_story = require('../controllers/story')
var passport = require('passport');

router.get('/new', controller_story.new );
//router.get('/list', passport.authenticate('local'), controller_story.list );
router.get('/list', controller_story.list );
router.post('/add', controller_story.add );
router.get('/remove/:id', controller_story.remove );
router.get('/view/:id', controller_story.view );
router.get('/view/:id/sentence/:sentence_id', controller_story.view );
router.get('/view/:id/sentences', controller_story.view_sentences );

module.exports = router;
