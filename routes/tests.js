var express = require('express');
var router = express.Router();
var controller_test = require('../controllers/test')

router.get('/run', controller_test.run );

module.exports = router;
