Sam was a dog.

In a house was a dog named Sam.

Sam was a fast dog in a house.

In the kitchen of the house was Sam, the fast dog.

And Kitty the cat.

Kitty and Sam were eating with June. In the kitchen. In a house.

The house of June, Sam and Kitty was in the country.

The farm of the fast dog, Sam, was in the country.

Who lived in the house in the country on a farm, and were eating in the kitchen?

Some cat and a dog were living in a country house, and their names were Kitty and Sam, and they both lived with June.

June had some parents.

June's parents owned the house with the fast dog.

Who killed June's father's mother?

Who was in the garden? And were they alive?

Someone in the garden had killed someone. Who were they?

The fast dog and the cool cat went outside to the garden after leaving the house.

In the garden was a tree.

On the tree was a rope.

Grandma was close to the rope. Very close. So close.

By the river, Sam and Kitty played.

The fish liked to play with Kitty.

A dark fish was in the river near Kitty.

Kitty did not see the dark fish under the dark cloud.

Sam saw something moving near Kitty.

There was a change in the sky.

A dark cloud moved slowly.

The sun did not object.

The dark fish got closer.

Sam could feel the air.

It was a cooler air.

And it was on his neck. This air.

Someone made Sam move his head towards Kitty. And the dark fish.

The dark fish decided to move away, back into the shadows.

Sam could use his fastness when he needed.

He could run fast to the barn.

He could catch the food when it got near his face.

He could know that someone was outside.

He could smell a stranger. Fast.

But not this time.

This time, there was no difference for his speed to catch.

Someone had killed Grandma. 

And he was slow to notice. Too slow.

Some nameless someone had killed Grandma.

Was it human?

Was it feline?

The dark vegetables in the garden were green.

And full of nutrients.

Kitty liked to chew on the greens.

Sam did not like greens as much as Kitty liked greens.

The greens which Kitty liked were not liked as much by Sam.

And that's OK. Be angry.

Sam did eat carrots.

He ate carrots that June gave to him as a present.

On his birthday, June gave him some carrots. But not as a present.

For his birthday, Sam got some meat.

Elsie was a cow who lived in the barn.

That was a long, long time ago.

Because for every one human year, there are seven dog years.

And no one knows how many years a cat has in relation to one human year.

And no one seems to care.

But June cared about Kitty in ways no one else did.

She was a sensitive child, June.

With a fast mind.

And a fast mouth.

June's parents often thought June was like her grandmother.

Who was in the garden.

Next to the greens and carrots. And tomatoes too.

The dark cloud moved back into the shadows.

The sun did not object.


